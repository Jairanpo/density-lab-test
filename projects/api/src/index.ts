import express, { Request, Response, NextFunction } from "express";
import knex from "./datasources/postgres";
import cors from "cors";

const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());

app.get("/", async (req, res) => {
  res.send("Reaching default express endpoint");
});

app.get("/comments", async (req, res) => {
  const data = await knex.select("*").from("comments");
  return res.status(200).json(data);
});

app.post("/comment/edit/:id", async (req, res) => {
  const commentId = req.params.id;
  console.log("server reached")

  try {
    if (!req.body.comment) {
      return res.status(400).json({ message: "Missing update payload" });
    }

    const values = (await knex("comments")
      .where({ id: commentId })
      .update("comment", req.body.comment, ["id", "comment", "email"]))[0];

    return res.status(200).json(values);
  } catch (err) {
    if (err instanceof Error) {
      return res.status(500).send(err.message);
    }
    return res.status(500).send("unknown error");
  }
});

app.delete("/comment/:commentId", async (req, res) => {
  const commentId = req.params.commentId;
  try {
    await knex("comments").where("id", commentId).del();
    return res.status(200).send("Comment deleted");
  } catch (err) {
    if (err instanceof Error) {
      return res.status(500).send(err.message);
    }
  }
});

app.post("/comment", async (req, res) => {
  try {
    if (!req.body.email || !req.body.comment) {
      return res.status(400).json({ message: "Empty email or comment body" });
    }
    const values = (
      await knex("comments")
        .insert({
          email: req.body.email,
          comment: req.body.comment,
        })
        .returning(["id", "email", "comment"])
    )[0];
    return res.status(200).json(values);
  } catch (err) {
    if (err instanceof Error) {
      return res.status(500).send(err.message);
    }
    return res.status(500).send("unknown error");
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
