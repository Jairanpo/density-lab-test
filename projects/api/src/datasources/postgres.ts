import knex from "knex";

const pgKnex = knex({
  client: 'pg',
  connection:{
    host: 'dev-db',
    user: 'postgres',
    password: 'mypass',
    database: 'db',
    port: 5432
  }
})

export default pgKnex


