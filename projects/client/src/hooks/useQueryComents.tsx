import { useEffect, useState } from "react";
import axios from "axios";
import { Comment } from "../context/main-context";

const baseUrl = "http://localhost:8080/api";

export default function useQueryComments() {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<null | Array<Comment>>(null);
  const [error, setError] = useState<Error | null>(null);

  const fetchComments = async () => {
    setLoading(true);
    try {
      const res = await axios.get(`${baseUrl}/comments`);
      setData(res.data)
    } catch (err) {
      if (err instanceof Error) {
        setError(err);
      }
    }
    setLoading(false);
  };

  return {
    loading,
    data,
    error,
    fetchComments,
  };
}
