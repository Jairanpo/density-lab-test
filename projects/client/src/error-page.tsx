import { useRouteError } from "../node_modules/react-router-dom/dist/index";

export default function ErrorPage() {
  const error = useRouteError();
  console.log(error);
  return (
    <div id="error-page">
      <div>Ha ocurrido un error</div>
      <div>
        <i>{error.statusText || error.message}</i>
      </div>
    </div>
  );
}
