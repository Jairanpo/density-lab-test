export type Comment = {
  id: number
  email: string
  comment: string
}
