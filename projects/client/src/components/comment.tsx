import styled from "styled-components";
import { ChangeEvent, MouseEvent, useState } from "react";
import axios from "axios";
import { Comment as CommentType} from "../types/comment-types";
import { API_URL } from "../shared/constants";
import { useMainContext } from "../context/main-context";
import { TextArea } from "../shared/styles";


const Comment = (props: {data: CommentType}) =>{
  const {email, id} = props.data
  const {updateComment, deleteComment} = useMainContext()
  const [editMode, setEditMode] = useState(false)
  const [comment, setComment] = useState(props.data.comment)

  const handleDelete = async(e: MouseEvent<HTMLButtonElement>)=>{
    e.preventDefault()
    try{
      await axios.delete(`${API_URL}/comment/${id}`)
      deleteComment(id) 
    }catch(err){
      throw err        
    }
  }
  
  const handleEdit = async(e: MouseEvent<HTMLButtonElement>)=>{
    e.preventDefault()
    try{
      const values = await axios.post(
        `${API_URL}/comment/edit/${id}`, 
        {comment},
        {'Content-Type': 'application/json'}
      )
      updateComment(values.data)
      setEditMode(false)
    }catch(err){
      throw err        
    }
  }

  const toggleEditMode = ()=>{
    setEditMode(!editMode)
  }

  return (
    <Wrapper>
      <Author>{email}</Author>
      <Body>{
        !editMode ? <div>{comment}</div>:
        <TextArea
          value={comment}
          onChange={(e: ChangeEvent<HTMLTextAreaElement>)=>{
            e.preventDefault()
            setComment(e.target.value)
          }}
        />
      }</Body>
      <Buttons>
        <Button onClick={toggleEditMode}>{
          editMode ? "Cancel" : "Edit"
        }</Button>
      {
        editMode ?
        <Button onClick={handleEdit}>Submit</Button> :
        <Button onClick={handleDelete}>Delete</Button>
      }
      </Buttons>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: grid;
  grid: auto-flow min-content / repeat(4, 1fr);
  gap: .4rem;
`;

const Author = styled.div`
  font-weight: bold;
  font-size: 1.2rem;
  grid-column-start: 1;
  grid-column-end: 3;
`

const Body = styled.div`
  font-weight: bold;
  font-size: .9rem;
  grid-column-start: 1;
  grid-column-end: 5;
  border-bottom: 1px solid;
  border-color: #444; 
  min-height: 40px;
`

const Buttons = styled.div`
  grid-column-start: 3;
  grid-column-end: 5;
  display: flex;
  justify-content: right;
  gap: .2rem;
`

const Button = styled.button`
`

export default Comment
