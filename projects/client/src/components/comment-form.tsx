import axios from "axios";
import styled from "styled-components";
import { useFormik } from "formik";
import * as Yup from "yup";
import useQueryComments from "../hooks/useQueryComents";
import { useMainContext } from "../context/main-context";
import { Comment } from "../types/comment-types";
import { API_URL } from "../shared/constants";


const validationSchema = Yup.object({
  email: Yup.string().email().required("Your email is required"),
  comment: Yup.string().required("Comment required"),
});

export default function CommentForm() {
  const { addComment } = useMainContext();

  const formik = useFormik({
    validationSchema,
    initialValues: {
      id: null,
      email: "",
      comment: "",
    },
    onSubmit: async (values: Comment) => {
      const res = await axios.post(`${API_URL}/comment`, values, {
        "Content-Type": "application/json",
      });
      if (res.status === 200) {
        addComment(res.data);
      }
    },
  });

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <FormLayout>
          <FullWidth>
            <label htmlFor="email" />
            <input
              type="email"
              id="email"
              name="email"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
              placeholder="Email"
            />
            {formik.errors.email && formik.touched.email ? (
              <div>{formik.errors.email}</div>
            ) : null}
          </FullWidth>
          <FullWidth>
            <label htmlFor="comment" />
            <textarea
              id="comment"
              name="comment"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
              placeholder="Add a comment..."
            />
            {formik.errors.comment && formik.touched.comment ? (
              <div>{formik.errors.comment}</div>
            ) : null}
          </FullWidth>
          <EleventhCol>
            <button type="submit">Comment</button>
          </EleventhCol>
        </FormLayout>
      </form>
    </>
  );
}

const FormLayout = styled.div`
  display: grid;
  gap: 1rem;
  grid: auto-flow min-content / repeat(12, 1fr);
  input {
    padding: 0.6rem;
    font-size: 1.2rem;
    width: 100%;
  }
  textarea {
    font-size: 1.2rem;
    padding: 0.6rem;
    min-height: 100px;
    width: 100%;
  }
`;
const FullWidth = styled.div`
  grid-column-start: 1;
  grid-column-end: 13;
`;
const EleventhCol = styled.div`
  grid-column-start: 11;
  grid-column-end: 13;
`;
