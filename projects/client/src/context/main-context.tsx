import { createContext, ReactNode, useContext } from "react";
import { observable } from "mobx";
import { Comment } from "../types/comment-types";

export type MainContext = {
  values: {
    comments: Array<Comment>;
  };
  comments: Array<Comment>;
  setComments: (values: Array<Comment>) => void;
  addComment: (value: Comment) => void;
  deleteComment: (id: number) => void;
  updateComment: (values: Comment) => void;
};

export const context = createContext<MainContext | null>(null);

context.displayName = "MainContext";

const store = observable({
  values: {
    comments: [],
  },
  get comments() {
    return store.values.comments;
  },
  addComment(value: Comment) {
    store.values.comments.push(value);
  },
  setComments(values: Array<Comment>) {
    store.values.comments = values;
  },
  deleteComment: (commentId: number) => {
    store.values.comments = store.values.comments.filter((c: Comment) => {
      return c.id != commentId;
    });
  },
  updateComment(comment: Comment) {
    store.values.comments.forEach((c: Comment, i: number) => {
      if (c.id == comment.id) {
        store.values.comments[i] = comment
      }
    });
  },
});

export const useMainContext = () => {
  const ctx = useContext(context);
  if (!ctx) {
    throw new Error(
      "useMainContext should be withing the main context provider"
    );
  }
  return ctx;
};

export const provider = (props: { children: ReactNode }) => {
  const { children } = props;

  return <context.Provider value={store}>{children}</context.Provider>;
};
