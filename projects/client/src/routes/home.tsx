import styled from "styled-components";
import { observer } from "mobx-react-lite";
import {v4 as uuid} from 'uuid'
import useQueryComments from "../hooks/useQueryComents";
import { Comment as CommentType } from "../types/comment-types";
import Comment from "../components/comment";
import CommentForm from "../components/comment-form";
import { useMainContext } from "../context/main-context";
import {useEffect} from "react";
////////////////////////////////////////////////////////////////////////////////

const Home = observer(()=>{
  const { loading, data, error, fetchComments } = useQueryComments();
  const { setComments, comments } = useMainContext();

  useEffect(()=>{
    fetchComments()
  },[])

  useEffect(()=>{
    if(data){
      setComments(data)
    }
  }, [data])

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error found!</div>;
  }

  return (
    <Wrapper>
      <Title>Leave Comments</Title>
      <CommentForm />
      <CommentsSection>
        {comments?.map((c: CommentType) => {
          return <div key={uuid()}>
            <Comment data={c}/>
          </div>
        })}
      </CommentsSection>
    </Wrapper>
  );
})

const Wrapper = styled.div`
  margin: 1rem;
  max-width: 600px;
  display: grid;
  grid: flow-auto min-content / 1fr;
  gap: 1rem;
`;

const Title = styled.div`
  font-size: 1.4rem;
  font-weight: bold;
`


const CommentsSection = styled.div`
  display: grid;
  gap: 1.2rem;
`;

const Loading = styled.div`
  font-size: 2rem;
  &:before {
    content: "Loading...";
  }
`;

export default Home
