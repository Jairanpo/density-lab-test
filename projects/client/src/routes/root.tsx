import { Outlet, Link } from "../../node_modules/react-router-dom/dist/index";


export default function Root() {
  return (
    <>
      <div>
        <h1>My React App</h1>
        <Link to="/about">about</Link>
        <div>
          <Outlet />
        </div>
      </div>
    </>
  );
}

