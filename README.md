# Docker must be installed in order to run the application

## To start run:
```
docker compose up --build
```
## With Make:
```
make dev-env
```

This will trigger the compose service to be build.
The front end would be accessible through port `8080`
NGINX will take control of request to the backend 
services. 

